class TestaTributavel {
//Resposta das questões:
	//t.getSaldo não funciona pois T é do tipo tributavel,que não há um metodo getSaldo.
	
  public static void main(String[] args) {
    ContaCorrente cc = new ContaCorrente();
    cc.deposita(100);
    System.out.println(cc.calculaTributos());

    // testando polimorfismo:
    Tributavel t = cc;
    //t.getSaldo();
    System.out.println(t.calculaTributos());
  }
}