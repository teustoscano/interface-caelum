
public class Retangulo implements AreaCalculavel{

	private int h;
	private int b;
	
	public Retangulo(int h, int b){
		this.b = b;
		this.h = h;
	}

	@Override
	public double calculaArea() {
		// TODO Auto-generated method stub
		return b * h;
	}
}
